package xesj.app.base;
import java.io.IOException;
import java.util.List;
import org.springframework.stereotype.Service;
import xesj.xterm.keyboard.Button;
import static xesj.xterm.keyboard.ButtonId.*;
import xesj.xterm.keyboard.Keyboard;
import xesj.xterm.terminal.Terminal;
import xesj.tool.ThreadTool;

/**
 * A lenyomott billentyűk kiírása. Ezzel tesztelni lehet hogy jó volt-e a billentyű kódok meghatározása.
 */
@Service
public class ButtonPrint {
  
  /**
   * A lenyomott billentyűk kiírása.
   */
  public void print() throws IOException {
    
    // Képernyő törlése
    Terminal.clear(true);
    
    // Kiírás
    Terminal.printRn(
      "Billentyűzet kódok tesztelése. Nyomj le tetszőleges billentyűket, és figyeld a kiírást." + Terminal.RN +
      "A billentyűk olvasása előtt 1 másodperc lassítás van, hogy több billentyűt is le lehessen ütni." + Terminal.RN +  
      "Ha meg akarod szakítani a tesztelést, akkor nyomj \"" + Keyboard.getButton(CTRL_CHR_Q).getName() + "\" billentyűt."  
    );
    
    // Billentyűk bekérése, amíg nem nyom "Ctrl q" -t
    outer: 
    while (true) {
      List<Button> list = Keyboard.readButtons(true);
      for (int i = 0; i < list.size(); i++) {
        Button button = list.get(i);
        if (button.equals(Keyboard.getButton(CTRL_CHR_Q))) break outer; // Ctrl q
        Terminal.printRn(button.getName());
      }  
      ThreadTool.sleep(1000); // Direkt lassítás
    }  
    
  }
  
  // ====
}
