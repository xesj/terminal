package xesj.app.base;
import java.io.IOException;
import org.springframework.stereotype.Service;
import xesj.xterm.terminal.Terminal;
import xesj.tool.ThreadTool;
import xesj.xterm.keyboard.KeyboardJson;

/**
 * Billentyű kódok kiírása text-fájlba.
 */
@Service
public class ButtonCodesToTextFile {
  
  /**
   * Kiírás text fájlba.
   */
  public void txt() throws IOException {
    
    // Text fájl írása
    String txtFile = KeyboardJson.writeToCodeOrderTxt();
    
    // Információ kiírása
    Terminal.clear(true);
    Terminal.printRn("A text-fálba írás megtörtént. Fájl: " + txtFile);
    ThreadTool.sleep(5000);
    
  }
  
  // ====
}
