package xesj.app.base;
import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Service;
import xesj.xterm.keyboard.Button;
import xesj.xterm.keyboard.ButtonId;
import static xesj.xterm.keyboard.ButtonId.*;
import xesj.xterm.keyboard.Keyboard;
import xesj.app.system.Property;
import xesj.xterm.terminal.Terminal;

/**
 * A program lényegi működése.
 */
@Service
public class Menu {
  
  private static final DateTimeFormatter formatter = 
    DateTimeFormatter.ofPattern("uuuu.MM.dd HH:mm:ss").withZone(ZoneId.systemDefault());
  
  @Autowired Property property;
  @Autowired ButtonReadCode buttonReadCode;
  @Autowired ButtonPrint buttonPrint;
  @Autowired ButtonCodesToTextFile buttonCodesToTextFile;
  @Autowired TerminalShow terminalShow;
  @Autowired BuildProperties buildProperties;
  
  /**
   * Főmenü.
   */
  public void menu() throws IOException {
    
    // Előkészítés
    String keyboardFileWithPath = new File(property.getKeyboard()).getCanonicalPath();
    
    // Alapbillentyűk kódjainak bekérése
    printProgramName(keyboardFileWithPath);
    buttonReadCode.base();
    
    // Menü választó billentyű bekérése
    outer:
    while (true) {

      // Alapinformációk, és menü kiírása
      Terminal.clear(true);
      Keyboard.clear();
      printProgramName(keyboardFileWithPath);
      
      Terminal.printRn(
        "Menü:" + Terminal.RN +
        Terminal.RN +
        "1. Billentyűzet kódok meghatározása (amelyek még nem ismertek)" + Terminal.RN +
        "2. Billentyűzet tesztelése (helyesen vannak-e meghatározva a billentyűzet kódok)" + Terminal.RN +
        "3. Billentyűzet kódok kiírása text-fájlba kód sorrendben (kódelemzéshez)" + Terminal.RN +
        "4. Terminál tesztelése (helyes-e a megjelenítés)" + Terminal.RN +
        Terminal.RN +
        "Válassz a menüből a menü számának megadásával." + Terminal.RN +
        "Kilépés a programból: \"" + Keyboard.getButton(ButtonId.CTRL_CHR_Q).getName() + "\"."
      );  
      
      // Menü választó billentyű bekérése
      List<Button> buttonList = Keyboard.readButtons(true);
      if (buttonList.isEmpty()) continue; 
      Button button = buttonList.get(0);
      switch (button.getId()) {

        // Kilépés a programból
        case CTRL_CHR_Q:
          break outer; 

        // Összes billentyű kódjának bekérése
        case CHR_1:
          buttonReadCode.all();
          break;
      
        // Gépelt billentyű kódok kiírása
        case CHR_2:
          buttonPrint.print();
          break;

        // Billentyűzet kódok kiírása text-fájlba
        case CHR_3:
          buttonCodesToTextFile.txt();
          break;

        // Terminál bemutató
        case CHR_4:
          terminalShow.show();
          break;
        
      } // Switch vége
      
    } // Nagyciklus vége
    
  }
  
  /**
   * Terminál verziójának, és keyboard.json fájljának kiírása
   */
  private void printProgramName(String keyboardFileWithPath) {
    
    Terminal.printRn(
      "Terminal " + buildProperties.getVersion() + " - " + formatter.format(buildProperties.getTime()) + Terminal.RN +
      "Keyboard: " + keyboardFileWithPath + Terminal.RN
    );
    
  }
  
  // ====
}
