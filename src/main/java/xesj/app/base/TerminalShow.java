package xesj.app.base;
import xesj.app.util.TerminalShowCancelException;
import java.io.IOException;
import java.util.List;
import org.springframework.stereotype.Service;
import xesj.xterm.keyboard.Button;
import static xesj.xterm.keyboard.ButtonId.*;
import xesj.xterm.keyboard.Keyboard;
import xesj.xterm.terminal.CursorPosition;
import xesj.xterm.terminal.Terminal;
import xesj.tool.ThreadTool;
import xesj.xterm.terminal.BaseColor;
import xesj.xterm.terminal.CursorMove;
import xesj.xterm.terminal.Decoration;
import xesj.xterm.terminal.Scroll;
import xesj.xterm.terminal.TerminalPart;

/**
 * A terminál lehetőségeinek bemutatása.
 */
@Service
public class TerminalShow {
  
  /**
   * Bemutató.
   */
  public void show() throws IOException {
    
    try {
      
      // Előkészítés
      int funkcio = 1;

      // A terminál méretének meghatározása
      terminalSize(funkcio++);
      
      // Cursor mozgatása
      cursorMove(funkcio++);

      // Cursor pozicionálása
      cursorPosition(funkcio++);

      // Cursor elrejtése, megjelenítése
      cursorHideShow(funkcio++);
      
      // Terminál ablak egy részének törlése
      clearPart(funkcio++);
      
      // Scroll
      scroll(funkcio++);
      
      // Betűk színének beállítása
      setColor(funkcio++);
      
      // Betű háttérszínének beállítása
      setBackgroundColor(funkcio++);
        
      // Dekoráció
      decoration(funkcio++);
      
    }
    catch (TerminalShowCancelException tse) {
      // A show meg lett szakítva Ctrl-q billentyűvel
    }
    
  }
  
  /**
   * Enter, Esc, Ctrl-q billentyűk funkciójának kiírása.
   */
  private void button3print() {
    
    Terminal.printRn(
      Terminal.RN +
      Keyboard.getButton(FUN_ENTER).getName() + ": Funkció ismétlése." + Terminal.RN +
      Keyboard.getButton(FUN_ESC).getName() + ": Kilépés ebből a funkcióból." + Terminal.RN +
      Keyboard.getButton(CTRL_CHR_Q).getName() + ": Kilépés a terminál megjelenítés teszteléséből."
    );
    
  }

  /**
   * Enter, Esc, Ctrl-q billentyűk lenyomásának lekezelése.
   */
  private boolean button3read() throws IOException, TerminalShowCancelException {

    List<Button> buttons;
    while (true) {
      do {
        buttons = Keyboard.readButtons(true);
      }
      while (buttons.isEmpty());
      Button button = buttons.get(0);
      if (button == Keyboard.getButton(FUN_ENTER)) {
        // Enter
        return true;
      }
      if (button == Keyboard.getButton(FUN_ESC)) {
        // Esc
        return false;
      }
      if (button == Keyboard.getButton(CTRL_CHR_Q)) {
        // Ctrl-q
        throw new TerminalShowCancelException();
      }
    }

  }
  
  /**
   * A terminál méretének meghatározása.
   */
  private void terminalSize(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Méret meghatározás
      Terminal.setCursorPosition(999, 999);
      CursorPosition pos = Terminal.getCursorPosition();

      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(
        funkcio + ". funkció: Terminál méretének meghatározása (ablak karakterszám szerint)." + Terminal.RN +
        "Sorok száma: " + pos.row + Terminal.RN +
        "Oszlopok száma: " + pos.column + Terminal.RN +
        "Méretezd át az ablakot, és ismételd a funkciót!"
      );
      button3print();
    } 
    while(button3read());
    
  }
  
  /**
   * Cursor eltolása.
   */
  private void cursorMove(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Előkészítés
      long sleep = 500;

      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(
        funkcio + ". funkció: Cursor mozgatása a csillag karakterek mentén." + Terminal.RN +
        "*            *      *" + Terminal.RN +
        "                     " + Terminal.RN +
        "*            *      *"
      );
      button3print();
      
      // Mozgatás
      Terminal.setCursorPosition(2, 1); ThreadTool.sleep(sleep);
      // 2. csillag
      Terminal.cursorMove(CursorMove.RIGHT, 13); ThreadTool.sleep(sleep);
      // 3. csillag
      Terminal.cursorMove(CursorMove.RIGHT, 7); ThreadTool.sleep(sleep);
      // 6. csillag
      Terminal.cursorMove(CursorMove.DOWN, 2); ThreadTool.sleep(sleep);
      // 5. csillag
      Terminal.cursorMove(CursorMove.LEFT, 7); ThreadTool.sleep(sleep);
      // 2. csillag
      Terminal.cursorMove(CursorMove.UP, 2); ThreadTool.sleep(sleep);
      // 1. csillag
      Terminal.cursorMove(CursorMove.ROWSTART, 48); ThreadTool.sleep(sleep);
      // 3. csillag
      Terminal.cursorMove(CursorMove.RIGHT, 20); ThreadTool.sleep(sleep);
      // 4. csillag
      Terminal.cursorMove(CursorMove.DOWN_ROWSTART, 2); ThreadTool.sleep(sleep);
      // 6. csillag
      Terminal.cursorMove(CursorMove.RIGHT, 20); ThreadTool.sleep(sleep);
      // 1. csillag
      Terminal.cursorMove(CursorMove.UP_ROWSTART, 2); ThreadTool.sleep(sleep);
    } 
    while(button3read());
    
  }
  
  /**
   * Cursor pozícionálása.
   */
  private void cursorPosition(int funkcio) throws IOException, TerminalShowCancelException {
    
    // Előkészítés
    long sleep = 500;

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(
        funkcio + ". funkció: Cursor pozícionálása a csillag karakterek mentén." + Terminal.RN +
        "  *            *" + Terminal.RN +
        "                " + Terminal.RN +
        "        *       " + Terminal.RN +
        "                " + Terminal.RN +
        "  *            *"
      );
      button3print();
      
      // 1. csillag
      Terminal.setCursorPosition(2, 3); ThreadTool.sleep(sleep);
      // 3. (középső) csillag
      Terminal.setCursorPosition(4, 9); ThreadTool.sleep(sleep);
      // Pozíció mentése
      Terminal.saveCursorPosition();
      // 2. csillag
      Terminal.setCursorPosition(2, 16); ThreadTool.sleep(sleep);
      // 3. (középső) csillag
      Terminal.restoreCursorPosition(); ThreadTool.sleep(sleep);
      // 4. csillag
      Terminal.setCursorPosition(6, 3); ThreadTool.sleep(sleep);
      // 3. (középső) csillag
      Terminal.restoreCursorPosition(); ThreadTool.sleep(sleep);
      // 5. csillag
      Terminal.setCursorPosition(6, 16); ThreadTool.sleep(sleep);
      // 3. (középső) csillag
      Terminal.restoreCursorPosition(); ThreadTool.sleep(sleep);
      // 1. csillag
      Terminal.setCursorPosition(2, 3); ThreadTool.sleep(sleep);
      // 2. csillag
      Terminal.setCursorColumn(16); ThreadTool.sleep(sleep);
      // 1. csillag
      Terminal.setCursorColumn(3);
    } 
    while(button3read());
    
  }
  
  /**
   * Cursor elrejtése, megjelenítése.
   */
  private void cursorHideShow(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(funkcio + ". funkció: Cursor elrejtése, és megjelenítése.");
      button3print();
      
      // Elrejtés
      Terminal.cursorHide(); ThreadTool.sleep(3000);
      // Megjelenítés
      Terminal.cursorShow();
    } 
    while(button3read());
    
  }
  
  /**
   * Terminál ablak egy részének törlése.
   */
  private void clearPart(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      clearPartPrint(funkcio);
      ThreadTool.sleep(3000);
      
      // Törlés a cursor-tól jobbra a sorban
      Terminal.setCursorPosition(3, 8);
      Terminal.clearPart(TerminalPart.CURSOR_TO_ROWEND);
      ThreadTool.sleep(2000);
      
      // Egész sor törlése
      Terminal.setCursorPosition(5, 11);
      Terminal.clearPart(TerminalPart.CURSOR_ROW);
      ThreadTool.sleep(2000);
      
      // Törlés a cursor-tól balra a sorban
      Terminal.setCursorPosition(7, 35);
      Terminal.clearPart(TerminalPart.ROWSTART_TO_CURSOR);
      ThreadTool.sleep(2000);
      
      // Törlés a képernyő elejétől a cursorig
      Terminal.setCursorPosition(4, 7);
      Terminal.clearPart(TerminalPart.START_TO_CURSOR);
      ThreadTool.sleep(2000);

      // Törlés a cursortól a képernyő végéig
      Terminal.clearPart(TerminalPart.CURSOR_TO_END);
      ThreadTool.sleep(2000);
      
      // Kiírás
      clearPartPrint(funkcio);
    } 
    while(button3read());

  }
  
  /**
   * clearPart() kiírása
   */
  private void clearPartPrint(int funkcio) {  

    Terminal.clear(true);
    Terminal.printRn(
      funkcio + ". funkció: Terminál ablak részeinek törlése." + Terminal.RN +
      "A csillag karakterek kerülnek törlésre." + Terminal.RN +
      "abcdefg***********************************" + Terminal.RN +
      "abcdefgHIJKLMNO" + Terminal.RN +
      "******************************************" + Terminal.RN +
      "abcdefg" + Terminal.RN +
      "***********************************abcdefg"
    );
    button3print();

  }

  /**
   * Scroll
   */
  private void scroll(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(funkcio + ". funkció: Scroll bemutatása.");
      button3print();
      ThreadTool.sleep(2000);
      
      // Scroll lefelé
      for (int i = 1; i <= 5; i++) {
        Terminal.scroll(Scroll.DOWN, 2);
        ThreadTool.sleep(1000);
      }

      // Scroll lefelé
      for (int i = 1; i <= 5; i++) {
        Terminal.scroll(Scroll.UP, 2);
        ThreadTool.sleep(1000);
      }
      
    } 
    while(button3read());

  }
  
  /**
   * Betűk színének beállítása.
   */
  private void setColor(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(funkcio + ". funkció: Betűk színének megjelenítése.");
      
      // A 8 alapszín + világos változatuk (16 szín)
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("A 8 alapszín, és alatta a világos változatuk:");
      Terminal.setColor(BaseColor.BLACK); Terminal.print("fekete ");
      Terminal.setColor(BaseColor.RED); Terminal.print("piros ");
      Terminal.setColor(BaseColor.GREEN); Terminal.print("zöld ");
      Terminal.setColor(BaseColor.YELLOW); Terminal.print("sárga ");
      Terminal.setColor(BaseColor.BLUE); Terminal.print("kék ");
      Terminal.setColor(BaseColor.MAGENTA); Terminal.print("magenta ");
      Terminal.setColor(BaseColor.CYAN); Terminal.print("ciánkék ");
      Terminal.setColor(BaseColor.WHITE); Terminal.print("fehér ");
      Terminal.printRn("");
      Terminal.setColor(BaseColor.LIGHT_BLACK); Terminal.print("fekete ");
      Terminal.setColor(BaseColor.LIGHT_RED); Terminal.print("piros ");
      Terminal.setColor(BaseColor.LIGHT_GREEN); Terminal.print("zöld ");
      Terminal.setColor(BaseColor.LIGHT_YELLOW); Terminal.print("sárga ");
      Terminal.setColor(BaseColor.LIGHT_BLUE); Terminal.print("kék ");
      Terminal.setColor(BaseColor.LIGHT_MAGENTA); Terminal.print("magenta ");
      Terminal.setColor(BaseColor.LIGHT_CYAN); Terminal.print("ciánkék ");
      Terminal.setColor(BaseColor.LIGHT_WHITE); Terminal.print("fehér ");
      Terminal.printRn("");
      
      // A 256 szín
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("A 256 szín, 0-255 színkóddal megadva:");
      for (int c = 0; c < 256; c++) {
        Terminal.setColor(c);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      
      // A 16 millió szín RGB-vel megadva
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("RGB-vel megadott színek:");
      for (int i = 0; i < 256; i++) {
        Terminal.setColor(i, i, i);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      for (int i = 0; i < 256; i++) {
        Terminal.setColor(0, 0, i);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      for (int i = 0; i < 256; i++) {
        Terminal.setColor(i, (85 + i) % 256, (170 + i) % 256);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      
      // Billentyűk kiírása      
      Terminal.reset();
      button3print();
    } 
    while(button3read());
    
  }
  
  /**
   * Betűk háttérszínének beállítása.
   */
  private void setBackgroundColor(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(funkcio + ". funkció: Betűk háttérszínének megjelenítése.");
      
      // A 8 alapszín + világos változatuk (16 szín)
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("A 8 alap háttérszín, és alatta a világos változatuk:");
      Terminal.setBackgroundColor(BaseColor.BLACK); Terminal.print(" fekete ");
      Terminal.setBackgroundColor(BaseColor.RED); Terminal.print(" piros ");
      Terminal.setBackgroundColor(BaseColor.GREEN); Terminal.print(" zöld ");
      Terminal.setBackgroundColor(BaseColor.YELLOW); Terminal.print(" sárga ");
      Terminal.setBackgroundColor(BaseColor.BLUE); Terminal.print(" kék ");
      Terminal.setBackgroundColor(BaseColor.MAGENTA); Terminal.print(" magenta ");
      Terminal.setBackgroundColor(BaseColor.CYAN); Terminal.print(" ciánkék ");
      Terminal.setBackgroundColor(BaseColor.WHITE); Terminal.print(" fehér ");
      Terminal.printRn("");
      Terminal.setBackgroundColor(BaseColor.LIGHT_BLACK); Terminal.print(" fekete ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_RED); Terminal.print(" piros ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_GREEN); Terminal.print(" zöld ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_YELLOW); Terminal.print(" sárga ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_BLUE); Terminal.print(" kék ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_MAGENTA); Terminal.print(" magenta ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_CYAN); Terminal.print(" ciánkék ");
      Terminal.setBackgroundColor(BaseColor.LIGHT_WHITE); Terminal.print(" fehér ");
      Terminal.printRn("");
      
      // A 256 szín
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("A 256 háttérszín, 0-255 színkóddal megadva:");
      for (int c = 0; c < 256; c++) {
        Terminal.setBackgroundColor(c);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      
      // A 16 millió szín RGB-vel megadva
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("RGB-vel megadott háttérszínek:");
      for (int i = 0; i < 256; i++) {
        Terminal.setBackgroundColor(i, i, i);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      for (int i = 0; i < 256; i++) {
        Terminal.setBackgroundColor(0, 0, i);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      for (int i = 0; i < 256; i++) {
        Terminal.setBackgroundColor(i, (85 + i) % 256, (170 + i) % 256);
        Terminal.print("*");
      }  
      Terminal.printRn("");
      
      // Betűszín, és betű háttérszín keverése
      Terminal.reset();
      Terminal.printRn("");
      Terminal.printRn("Betűszín, és betű háttérszín keverése:");
      Terminal.setColor(BaseColor.BLACK);
      Terminal.setBackgroundColor(BaseColor.RED);
      Terminal.print(" Piros alapon fekete betűk " + Terminal.RN);
      Terminal.setColor(BaseColor.LIGHT_BLUE);
      Terminal.setBackgroundColor(BaseColor.LIGHT_YELLOW);
      Terminal.print(" Világos sárga alapon világos kék betűk " + Terminal.RN);
      
      // Billentyűk kiírása      
      Terminal.reset();
      button3print();
    } 
    while(button3read());

  }

  /** 
   * Dekoráció: vastag, dőlt, aláhúzott, ellentétes betű/háttér
   */
  private void decoration(int funkcio) throws IOException, TerminalShowCancelException {

    do {
      // Kiírás
      Terminal.clear(true);
      Terminal.printRn(funkcio + ". funkció: Betűk dekorációja.");

      // Bold
      Terminal.setDecoration(Decoration.BOLD);
      Terminal.printRn("Vastag betű");
      Terminal.reset();

      // Italic
      Terminal.setDecoration(Decoration.ITALIC);
      Terminal.printRn("Dőlt betű");
      Terminal.reset();

      // Underline
      Terminal.setDecoration(Decoration.UNDERLINE);
      Terminal.printRn("Aláhúzott betű");
      Terminal.reset();

      // Opposite
      Terminal.setDecoration(Decoration.OPPOSITE);
      Terminal.printRn("Ellentétes megjelenés (betűszín - háttérszín megcserélése)");
      Terminal.reset();
      
      // Bold + Italic + Underline
      Terminal.setDecoration(Decoration.BOLD);
      Terminal.setDecoration(Decoration.ITALIC);
      Terminal.setDecoration(Decoration.UNDERLINE);
      Terminal.printRn("Vastag + dőlt + aláhúzott betű");
      Terminal.reset();

      // Billentyűk kiírása      
      button3print();
    } 
    while(button3read());

  }
  
  // ====
}
