package xesj.app.base;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import xesj.xterm.keyboard.Button;
import xesj.xterm.keyboard.ButtonId;
import xesj.xterm.keyboard.Keyboard;
import static xesj.xterm.keyboard.ButtonId.*;
import xesj.xterm.keyboard.ButtonFind;
import xesj.xterm.keyboard.KeyboardJson;
import xesj.xterm.terminal.Terminal;
import xesj.tool.ThreadTool;

/**
 * Beolvassa a billentyűzetről a billentyűk kódját.
 */
@Service
public class ButtonReadCode {
  
  /**
   * Beolvassa a billentyűzetről az alapvető billentyűk kódját. Ha minden kód megvan, akkor nem csinál semmit.
   * Az alapvető billentyűk (ENTER, ESC, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, CTRL-Q) kódját olvassa be, 
   * de csak azokat, melyek még nem ismertek.
   */
  public void base() throws IOException {
    
    // Id lista előállítása
    ButtonId[] baseIds = new ButtonId[]{
      FUN_ENTER, FUN_ESC, CHR_0, CHR_1, CHR_2, CHR_3, CHR_4, CHR_5, CHR_6, CHR_7, CHR_8, CHR_9, CTRL_CHR_Q
    };
    List<ButtonId> ids = new ArrayList<>();
    for (ButtonId id: baseIds) {
      Button button = Keyboard.getButton(id);
      if (button.getCode() == null) {
        ids.add(id);
      }
    }
    
    // Minden kód megvan ? Nincs teendő ?
    if (ids.isEmpty()) return; 
    
    // Információ kiírása a felhasználónak
    Terminal.printRn(
      "Az alapbillentyűk kódja hiányos, ezért ezeket meghatározzuk." + Terminal.RN +
      "Nyomd le az alábbi billentyűket:"
    );
    
    // Code bekérés
    for (int i = 0; i < ids.size(); i++) {
      Button button = Keyboard.getButton(ids.get(i));
      Keyboard.clear();
      Terminal.print(button.getName() + "  ->  ");
      String code = Keyboard.readCode();
      Terminal.print(code);

      // A kód egyediségének ellenőrzése
      Button otherButton = Keyboard.getButton(code);
      if (otherButton != null) {
        // Rossz kód
        Terminal.print("  Hiba: ez a kód ehhez a billentyűhöz tartozik: " + otherButton.getName());
        i--;
      }
      else {
        // Jó kód, a kód tárolása
        button.setCode(code);
        Keyboard.putButton(code, button);
        KeyboardJson.writeToFile();
      }
      Terminal.printRn("");
    }
    
    // Search lista előállítása
    ButtonFind.createSearchList();
    
  }
  
  /**
   * Beolvassa a billentyűzetről az összes olyan billentyű kódját, amely még nem ismert (ahol code == null).
   * Ha minden kód megvan, akkor üzenetet ír, és nem csinál semmit.
   * Speciális billentyűket is le lehet nyomni a kódok bekérése közben. Ezek a következők:
   *   ESC = nem tudjuk a kódot megadni (a kód "NA" lesz)
   *   CTRL Q = megszakítjuk billentyűkód bekérő all() metódust
   */
  public void all() throws IOException {
    
    // Előkészítés
    Terminal.clear(true);
    Button 
      ctrlqButton = Keyboard.getButton(CTRL_CHR_Q),
      escButton = Keyboard.getButton(FUN_ESC);
    List<ButtonId> ids = new ArrayList<>();
    for (Button button: Keyboard.getButtonList()) {
      if (button.getCode() == null) {
        ids.add(button.getId());
      }
    }
    
    // Minden kód megvan ? Nincs teendő ?
    if (ids.isEmpty()) {
      Terminal.printRn("Már minden billentyű kódja meg van határozva.");
      ThreadTool.sleep(5000);
      return;
    } 
    
    // Információ kiírása a felhasználónak
    Terminal.printRn(
      Keyboard.getButtonList().size() + " billentyűből még " + ids.size() + " " +
      "kódja ismeretlen, ezért ezeket meghatározzuk." + Terminal.RN +
      "Ha egy billentyű fizikailag nem nyomható le, vagy a lenyomásakor nem keletkezik kód," + Terminal.RN +
      "vagy a kód már foglalt más billentyű számára, akkor nyomj \"" +  
      Keyboard.getButton(FUN_ESC).getName() + "\" billentyűt." + Terminal.RN +
      "Ha meg akarod szakítani a kód bekérést, akkor nyomj \"" + 
      Keyboard.getButton(CTRL_CHR_Q).getName() + "\" billentyűt." + Terminal.RN +
      "Nyomd le az alábbi billentyűket:"
    );
    
    // Code bekérés
    for (int i = 0; i < ids.size(); i++) {
      Button button = Keyboard.getButton(ids.get(i));
      Keyboard.clear();
      Terminal.print(button.getName() + "  ->  ");
      String code = Keyboard.readCode();

      // Speciális billentyű ? Ctrl c ?
      if (code.equals(ctrlqButton.getCode())) {
        break; // Kilépés a ciklusból
      }

      // Speciális billentyű ? Esc ?
      if (code.equals(escButton.getCode())) {
        button.setCode(Button.NOT_AVAILABLE);
        Terminal.printRn(Button.NOT_AVAILABLE);
        continue;
      }

      // Kód kiírása
      Terminal.print(code);
      
      // A kód egyediségének ellenőrzése
      Button otherButton = Keyboard.getButton(code);
      if (otherButton != null) {
        // Rossz kód
        Terminal.print("  Hiba: ez már létező kód ehhez a billentyűhöz: " + otherButton.getName());
        i--;
      }
      else {
        // Jó kód, a kód tárolása
        button.setCode(code);
        Keyboard.putButton(code, button);
        KeyboardJson.writeToFile();
      }
      Terminal.printRn("");
    }
    
    // Search lista előállítása
    ButtonFind.createSearchList();
    
  }

  // ====
}
