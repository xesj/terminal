package xesj.app.util;

/**
 * Akkor váltódik ki, ha TerminalShow közben lenyomjuk a Ctrl-q billentyűt,
 * vagyis jelezzük, hogy félbe akarjuk szakítani a show-t.
 */
public class TerminalShowCancelException extends Exception {
  
  public TerminalShowCancelException() {}

}
