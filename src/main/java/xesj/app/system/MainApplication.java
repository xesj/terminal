package xesj.app.system;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Main application bean
 */
@Service
@Getter
public class MainApplication {

  /**
   * Application context nem managed bean-ek számára
   */
  @Getter
  @Setter
  private static ApplicationContext context;

  // ====
}
