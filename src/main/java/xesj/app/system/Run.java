package xesj.app.system;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "xesj.app")
public class Run {
  
  /**
   * Spring Boot inicializálás
   */
  public static void main(String[] args) throws Exception {
    
    SpringApplication.run(Run.class, args);

  }
  
  // ====
}
