package xesj.app.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import xesj.app.base.Menu;
import xesj.xterm.keyboard.Keyboard;
import xesj.xterm.terminal.Terminal;

/**
 * Main event listener.
 */
@Service
public class MainEventListener {
  
  @Autowired ApplicationContext context;
  @Autowired Menu menu;
  @Autowired Property property;

  @EventListener
  public void afterStart(ApplicationStartedEvent event) throws Exception {
    
    // Application context beállítása statikus változóba
    MainApplication.setContext(context);

    // Terminal tartalmának törlése
    Terminal.clear(true);
    
    // Keyboard használatának elkezdése
    Keyboard.start(property.getKeyboard());

    // Menü
    menu.menu();
    
  }  
  
  // ====
}
