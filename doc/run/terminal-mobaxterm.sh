#!/bin/bash
stty raw -echo
cd /drives/d/ws-netbeans/XESJ/terminal
/drives/d/tools/jdk-17.0.2/bin/java.exe \
  -jar ./target/terminal.jar \
  --app.keyboard=./doc/run/windows.mobaxterm.keyboard.json
stty -raw echo
