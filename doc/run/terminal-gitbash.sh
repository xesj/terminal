#!/bin/bash
stty raw -echo
chcp.com 65001
cd /d/ws-netbeans/XESJ/terminal
/d/tools/jdk-17.0.2/bin/java.exe \
  -jar ./target/terminal.jar \
  --app.keyboard=./doc/run/windows.gitbash.keyboard.json
stty -raw echo
