#!/bin/bash
stty raw -echo
cd /Users/xesj/ws-netbeans/terminal
/Library/Java/JavaVirtualMachines/temurin-17.jdk/Contents/Home/bin/java \
  -jar ./target/terminal.jar \
  --app.keyboard=./doc/run/macos.iterm2.keyboard.json
stty -raw echo
